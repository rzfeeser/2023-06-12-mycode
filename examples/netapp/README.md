# Ansible Netapp Solution

No inventory is necessary if you're only connecting to a single NetApp platform.

### Procedure

1) Read documentation on modules
ansible-doc -l netapp.ontap

2) Install python dependencies (requirements.txt is copied from vendor GitHub repo containing their collection)
python3 -m pip install -r requirements.txt

3) Run the playbook
ansible-playbook playbook_netapp.yml
